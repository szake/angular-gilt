'use strict';

angular.module('giltApp.main', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'main/main.html',
    controller: 'mainCtrl'
  });
}])

.controller('mainCtrl', ['$scope', '$http', function($scope, $http) {
  var requestUrl = 'https://api.gilt.com/v1/sales/active.json?apikey=' + $scope.apikey;

  $http.get(requestUrl).success(
    function(data){
      $scope.sales = data.sales;
      $scope.type = typeof $scope.sales;
    }
  );

}]);