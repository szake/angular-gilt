'use strict';

// Declare app level module which depends on views, and components
angular.module('giltApp', [
    'ngRoute',
    'giltApp.main',
    'giltApp.sales',
    'giltApp.sale',
    'giltApp.product',
    'giltApp.upcomingSales',
    'giltApp.upcomingSale',
    'giltApp.search'
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}])

.controller('indexCtrl', ['$scope', '$http', '$location', function($scope, $http, $location){
    $scope.apikey = '9cc6e1b63c39dd90f3d2f905e8d4b6404f2ec308fac89b924fe8500bb2fbde8d';

    $scope.searchRequest = '';
    $scope.searchSubmit = function(){
        if (!$scope.searchRequest) return;
        $location.path('/search').search({g: $scope.searchRequest});
    };
}]);

/* Class SALE */
/* -- none -- */

/* Class PRODUCT */
var ProductItem = function(data){
    this.id = data.id;
    this.name = data.name;
    this.brand = data.brand;
    this.description = data.content.description;
    this.material = data.content.material;
    this.attributes = data.skus[0].attributes; // an Array
    this.care = data.content.care_instructions;
    this.origin = data.content.origin;
    this.data = data.product;
    this.usual_price = data.skus[0].msrp_price;
    this.sale_price = data.skus[0].sale_price;
    this.units = data.skus[0].units_for_sale;

    var images = data.image_urls;
    var imagesKeys = Object.keys(images);

    for (var i = 0; i < imagesKeys.length; i++) {
        if (images[imagesKeys[i]][0].width >= 300 && images[imagesKeys[i]][0].width <= 600 && this.image == undefined) {
            this.image = images[imagesKeys[i]][0].url;
            break;
        }
    }

    if (!this.image) this.image = images[imagesKeys[0]][0].url;
};

/* Functions */
var createSaleTerm = function(date){
    var saleTerm = new Date(date),
        saleMonth = saleTerm.getMonth() + 1,
        saleDate = saleTerm.getDate(),
        saleHours = saleTerm.getHours(),
        saleMinutes = saleTerm.getMinutes();

    return  ((saleDate < 10) ? ('0' + saleDate) : saleDate) + '/' +
        ((saleMonth < 10) ? ('0' + saleMonth) : saleMonth) + '/' +
        (saleTerm.getFullYear()) + ' at ' +
        ((saleHours < 10) ? ('0' + saleHours) : saleHours) + ':' +
        ((saleMinutes < 10) ? ('0' + saleMinutes) : saleMinutes);
};


/* $scope.loadMoreButton = true;

 $http.get(requestUrl).then(
 function(data){
 var allSales = data.data.sales;

 var partList = [];
 var partSize = 18;
 var partShown = 0;

 if (allSales.length) {
 for (var i = 0; i < allSales.length; i += partSize) {
 var part = allSales.slice(i, i+partSize); // console.log(part);
 partList.push(part);
 }
 }

 if (allSales.length <= partSize) { $scope.loadMoreButton = false; }

 var shownSales = partList[partShown];
 $scope.sales = shownSales;

 var scrollPageOnMore = function(){
 $('html, body').animate({
 scrollTop: $(document).scrollTop() + $(window).height()/3
 }, 500);
 };

 $scope.loadMoreSales = function(){
 if (partShown >= (partList.length - 1)) { return; }

 $scope.sales = shownSales = shownSales.concat(partList[++partShown]);

 if (partShown >= (partList.length - 1)) {
 $scope.loadMoreButton = false;
 }

 scrollPageOnMore();
 };
 }
 );

 $scope.viewMoreAboutSale = function(sale){
 var requestUrl = 'https://api.gilt.com/v1/sales/' + sale.store + '/' + sale.sale_key + '/detail.json?apikey=' + $scope.apikey;
 var reference = location.href + ':' + sale.store + ':' + sale.sale_key;
 window.open(reference, '_self');
 }; */