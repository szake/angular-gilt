'use strict';

angular.module('giltApp.sales', ['ngRoute', 'ngAnimate'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/sales/:storeId', {
            templateUrl: 'sales/sales.html',
            controller: 'salesCtrl'
        });
}])

.controller('salesCtrl', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    var store = $routeParams.storeId;

    var requestUrl = 'https://api.gilt.com/v1/sales/' + store +  '/active.json?apikey=' + $scope.apikey;
    var salesList = [];

    $http
        .get(requestUrl)
        .then(function(response){
            var salesData = response.data.sales;

            for (var i = 0; i < salesData.length; i++) {
                salesList[i] = salesData[i];
            }
        });

    $scope.currentSales = salesList;
    $scope.salesStore = store;
}]);