'use strict';

angular.module('giltApp.sale', ['ngRoute', 'ngAnimate'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/sales/:storeId/:saleId', {
            templateUrl: 'sales/sale.html',
            controller: 'saleCtrl'
        });
}])

.controller('saleCtrl', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    var store = $routeParams.storeId;
    var saleKey = $routeParams.saleId;

    var requestUrl = 'https://api.gilt.com/v1/sales/' + store + '/' + saleKey + '/detail.json?apikey=' + $scope.apikey;

    var createSaleProducts = function(productReferences){
        var productList = [];

        for (var i = 0; i < productReferences.length; i++) {
            var requestUrl = productReferences[i] + '?apikey=' + $scope.apikey;

            $http.get(requestUrl).then(function(response){
                var product = response.data ? new ProductItem(response.data) : {};
                productList.push(product);
            });
        }
        return productList;
    };

    var loadSaleInfo = function(response) {
        $scope.sale = response;
        $scope.saleEnd = createSaleTerm($scope.sale.ends);
        $scope.saleProducts = createSaleProducts($scope.sale.products);
    };

    $http
        .get(requestUrl)
        .then(function(response){
            loadSaleInfo(response.data);
        });
}]);