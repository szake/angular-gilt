'use strict';

angular.module('giltApp.product', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/products/:productId', {
      templateUrl: 'product/product.html',
      controller: 'productCtrl'
    });
}])

.controller('productCtrl', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

  var productId = $routeParams.productId;
  var requestUrl = 'https://api.gilt.com/v1/products/' + productId + '/detail.json?apikey=' + $scope.apikey;

  $http
    .get(requestUrl)
    .then(function(response){
      $scope.productId = response.data;
    });

}]);