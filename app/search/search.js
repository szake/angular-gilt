'use strict';

angular.module('giltApp.search', ['ngRoute', 'ngAnimate'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/search', {
            templateUrl: 'search/search.html',
            controller: 'searchCtrl'
        });
}])

.controller('searchCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {

    var searchRequest = $location.search().g;

    var requestUrl = 'https://api.gilt.com/v1/products?q=' + searchRequest + '&apikey=' + $scope.apikey;
    var searchList = [];

    $http
        .get(requestUrl)
        .then(function(response){
            var searchData = response.data.products;

            for (var i = 0; i < searchData.length; i++) {
                searchList[i] = new ProductItem(searchData[i]);
            }
        });

    $scope.searchProducts = searchList;
}]);