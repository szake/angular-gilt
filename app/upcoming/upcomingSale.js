'use strict';

angular.module('giltApp.upcomingSale', ['ngRoute', 'ngAnimate'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/upcoming/:storeId/:saleId', {
            templateUrl: 'upcoming/upcomingSale.html',
            controller: 'upcomingSaleCtrl'
        });
}])

.controller('upcomingSaleCtrl', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    var store = $routeParams.storeId;
    var saleKey = $routeParams.saleId;

    var requestUrl = 'https://api.gilt.com/v1/sales/' + store + '/' + saleKey + '/detail.json?apikey=' + $scope.apikey;

    var loadSaleInfo = function(response) {
        $scope.sale = response;
        $scope.saleStart = createSaleTerm($scope.sale.begins);
        $scope.saleEnd = createSaleTerm($scope.sale.ends);
    };

    $http
        .get(requestUrl)
        .then(function(response){
            loadSaleInfo(response.data);
        });
}]);