'use strict';

angular.module('giltApp.upcomingSales', ['ngRoute', 'ngAnimate'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/upcoming', {
            templateUrl: 'upcoming/upcomingSales.html',
            controller: 'upcomingSalesCtrl'
        });
}])

.controller('upcomingSalesCtrl', ['$scope', '$http', function($scope, $http) {

    var requestUrl = 'https://api.gilt.com/v1/sales/upcoming.json?apikey=' + $scope.apikey;
    var upcomingList = [];

    $http
        .get(requestUrl)
        .then(function(response){
            var upcomingData = response.data.sales;

            for (var i = 0; i < upcomingData.length; i++) {
                upcomingList[i] = upcomingData[i];
            }
        });

    $scope.upcomingSales = upcomingList;
}]);